This submodule is separate from, does not require, drutopia_dev itself.

Indeed, it is mutually exclusive, because it has one identical configuration
file.

This could and probably should change if most of drutopia_dev is moved to a
`drutopia_dev_drutopia` submodule for the installation profile, and only the
features configuration is left in the parent module; that could then be removed
from this submodule:

Namely: `config/install/features.bundle.drutopia.yml`
